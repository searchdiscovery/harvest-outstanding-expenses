require 'sinatra'
require 'slim'
require 'harvested'

class HarvestApp < Sinatra::Base

  configure do
    set :sessions, true
    set :session_secret, 'badunkadunkSDI!!1'
    set :logging, ENV['RACK_ENV'] == 'production'
  end

  get '/' do
    unless session[:authenticated]
      if session[:email]
        @email = session[:email]
      end
      return slim :login
    end
    slim :expenses
  end

  post '/login' do
    session[:email] = params[:email]
    session[:password] = params[:password]
    session[:authenticated] = true
    redirect '/'
  end

  get '/logout' do
    session[:password] = nil
    session[:authenticated] = false;
    redirect '/'
  end

  get '/api/expenses' do
    content_type :json
    data = { expenses: [] }
    users = {}
    harvest = Harvest.client('searchdiscovery', session[:email], session[:password])
    harvest.users.all.each do |user|
      harvest.reports.expenses_by_user(user.id, Time.utc(2000, 1, 1), Time.now.utc).each do |expense|
        if !expense.is_closed && expense.locked_reason != "Expense is invoiced."
          expense = expense.to_hash
          expense[:links] = { user: user.id }
          users[user.id] = user.to_hash
          data[:expenses].push(expense)
        end
      end
    end
    data[:linked] = { users: users.values }
    data[:links] = {
      "expenses.users" => {
        "type" => "users"
      }
    }
    data.to_json;
  end
end

HarvestApp.run! if __FILE__ == $0
