$LOAD_PATH.unshift(File.dirname(__FILE__))
require 'rubygems'
require 'app'

# I have an extraordinarily hard time believing that there is no better way
# of doing this.

if ENV['RACK_ENV'] == 'production'
  module Rack
    class CommonLogger
      def info
        # do nothing
      end
      def call(env)
        # do nothing
        @app.call(env)
      end
    end
  end
  class Logger
    def info(x)
    end
    def debug(x)
    end
    def warn(x)
    end
    def call(env)
      # do nothing
      @app.call(env)
    end
  end
end

run HarvestApp
