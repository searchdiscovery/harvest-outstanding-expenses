$('.get-expenses').click(function () {
  var that = this;
  $('.expenses tr:not(.heading)').remove();
  $(this).button('loading');
  $.getJSON('/api/expenses', function (data) {
    $(that).button('reset');
    var table = toTable(toRows(data));
    $('.expenses').append(table);
  });
});

function indexArray (arr, key) {
  key = key || 'id';
  var dict = {};
  $.each(arr, function (i, elem) {
    dict[elem[key]] = elem;
  });
  return dict;
}

function toRows (data) {
  var users = indexArray(data.linked.users);
  return $.map(data.expenses, function (expense) {
    var employee = users[expense.links.user];
    return {
      project: expense.project_id,
      employee: employee.last_name + ', ' + employee.first_name,
      notes: expense.notes,
      date: expense.spent_at
    };
  });
}

function toTable (data) {
  return $.map(data, function (expense) {
    return '<tr>' + '<td><a target="_blank" href="https://searchdiscovery.harvestapp.com/projects/' + expense.project + '">' + expense.project + '</a></td><td>' + expense.employee + '</td><td>' + expense.notes + '</td><td>' + expense.date + '</td></tr>';
  }).join('');
}

